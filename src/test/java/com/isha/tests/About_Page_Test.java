package com.isha.tests;

import com.isha.steps.Isha_Website_Steps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
@RunWith(SerenityRunner.class)
public class About_Page_Test {
    @Managed (uniqueSession = false,clearCookies = ClearCookiesPolicy.BeforeEachTest)
    public WebDriver driver;

    @Steps
    Isha_Website_Steps  obj;
    @Title("This is First case Title")
    @Test
    public void navigate_About_Page_Test1() throws InterruptedException {

        obj.navigate_About_Page();
        System.out.println("this is first case");
    }
    @Title("This is second case Title")
    @Test
    public void navigate_About_Page_Test2() throws InterruptedException {

        obj.navigate_About_Page();
       // int a=10/0;
        System.out.println("this is second case");
    }
    @Title("This is third case Title")
    @Test
    public void navigate_About_Page_Test3() throws InterruptedException {

        obj.navigate_About_Page();
        System.out.println("this is second case");
    }


    @Title("This is fourth case Title")
    @Test
    public void navigate_About_Page_Test4( ) throws InterruptedException {

        obj.navigate_About_Page();
        int b=10/0;
        System.out.println("This is fourth case");
    }



}
