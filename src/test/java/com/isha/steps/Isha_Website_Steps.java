package com.isha.steps;

import com.isha.pages.About_Page;
import com.isha.pages.Base_Page;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class Isha_Website_Steps extends ScenarioSteps {

    Base_Page b1;
    About_Page a1;

    @Step("To navigate to About Page of Isha website")
    public About_Page navigate_About_Page() throws InterruptedException {
        return b1.navigate_About_Page();
    }

}
