package com.isha.pages;


import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Base_Page extends PageObject {

    private static final String ABOUT = "//*[@alt='isha-home-mystic_new']";

    public About_Page navigate_About_Page() throws InterruptedException {
        open();
        WebElement about_link = getDriver().findElement(By.xpath(ABOUT));
        getDriver().manage().window().maximize();
        Thread.sleep(5000);
        about_link.click();
        Thread.sleep(5000);
        return this.switchToPage(About_Page.class);
    }

}
